\defclass{Complex}
This class allows one to work with complex numbers of
the form $a + bi$, where $a$ is the \emph{real} part of
the number, \emph{b} is the imaginary part and
$i=\sqrt{-1}$.
\bigskip\hrule\bigskip
\begin{code}
public class Complex\begin{hide} {
private double realPart;
private double imagPart;\end{hide}
public Complex (double realPart, double imagPart)\begin{hide} {
this.realPart = realPart;
this.imagPart = imagPart;
}\end{hide}
\end{code}
\begin{tabb} Constructs a new {\tt Complex} number.
\param{realPart}{The real part corresponding to $a$}
\param{imagPart}{The imaginary part corresponding to $b$}
\end{tabb}
\begin{code}
public Complex add (Complex c)\begin{hide} {
realPart += c.realPart;
imagPart += c.realPart;
return this;
}\end{hide}

\end{code}
\begin{tabb} Adds two complex numbers.
\param{c}{The complex number to add to this one.}
\return{This object, allowing to perform more than one operation
on a single line of code.}
\end{tabb}
\begin{code}
// other methods...
public String toString()\begin{hide} {
return "(" + realPart + " + " + imagPart + "i)";
}
// Test code
public static void main(String[] args) {
	Complex c1 = new Complex(1, 2);
	Complex c2 = new Complex(2, 3);
	assert c1.add(c2).toString().equals("(3.0 + 4.0i)");
	System.out.println("PASS");
}
}\end{hide}
\end{code}
\begin{tabb}
 Converts the number to a {\tt String} of
the form {\tt a + bi}.
\end{tabb}


