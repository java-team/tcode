# comes from alltt
if ($alltt_rx) {
    $alltt_rx = 'vcode'."|$alltt_rx";
    $alltt_rx =~ s/\|{2,}/\|/g; $alltt_rx =~ s/\|$//g;
} else {
    $alltt_rx = 'vcode';
}

# everything here is inspired from other .perl files
# in the styles subdirectory of latex2html.
# There is no official documentation on how to implement
# new commands, except the ones that need to be ignored
# or passed to LaTeX to generate images.

&do_require_package ('alltt');

sub do_math_cmd_R {
   ('', '\\Re');
}

sub do_cmd_class {
   local($_) = @_;
   my $arg;

   $arg = &missing_braces unless (
    (s/$next_pair_pr_rx/$arg = $2;''/eo)
    ||(s/$next_pair_rx/$arg = $2;''/eo));

   join ('', "{\@link $arg}", $_);
}

sub do_cmd_externalclass {
   local($_) = @_;
   my ($package, $class);

   $package = &missing_braces unless (
    (s/$next_pair_pr_rx/$package = $2;''/eo)
    ||(s/$next_pair_rx/$package = $2;''/eo));

   $class = &missing_braces unless (
    (s/$next_pair_pr_rx/$class = $2;''/eo)
    ||(s/$next_pair_rx/$class = $2;''/eo));

   join ('', "{\@link $package.$class $class}", $_);
}

sub do_cmd_method {
   local($_) = @_;
   my ($method,$signature);

   $method = &missing_braces unless (
    (s/$next_pair_pr_rx/$method = $2;''/eo)
    ||(s/$next_pair_rx/$method = $2;''/eo));

   $signature = &missing_braces unless (
    (s/$next_pair_pr_rx/$signature = $2;''/eo)
    ||(s/$next_pair_rx/$signature = $2;''/eo));

   $signature = "($signature)" if (length ($signature) > 0);
   join ('', "{\@link #$method$signature $method}", $_);
}

sub do_cmd_externalmethod {
   local($_) = @_;
   my ($package, $class, $method, $signature);

   $package = &missing_braces unless (
    (s/$next_pair_pr_rx/$package = $2;''/eo)
    ||(s/$next_pair_rx/$package = $2;''/eo));

   $class = &missing_braces unless (
    (s/$next_pair_pr_rx/$class = $2;''/eo)
    ||(s/$next_pair_rx/$class = $2;''/eo));

   $method = &missing_braces unless (
    (s/$next_pair_pr_rx/$method = $2;''/eo)
    ||(s/$next_pair_rx/$method = $2;''/eo));

   $signature = &missing_braces unless (
    (s/$next_pair_pr_rx/$signature = $2;''/eo)
    ||(s/$next_pair_rx/$signature = $2;''/eo));

   $package .= '.' if (length($package) > 0);
   $signature = "($signature)" if (length ($signature) > 0);
   join ('', "{\@link $package$class#$method$signature $method}", $_);
}

sub do_cmd_clsexternalmethod {
   local($_) = @_;
   my ($package, $class, $method, $signature);

   $package = &missing_braces unless (
    (s/$next_pair_pr_rx/$package = $2;''/eo)
    ||(s/$next_pair_rx/$package = $2;''/eo));

   $class = &missing_braces unless (
    (s/$next_pair_pr_rx/$class = $2;''/eo)
    ||(s/$next_pair_rx/$class = $2;''/eo));

   $method = &missing_braces unless (
    (s/$next_pair_pr_rx/$method = $2;''/eo)
    ||(s/$next_pair_rx/$method = $2;''/eo));

   $signature = &missing_braces unless (
    (s/$next_pair_pr_rx/$signature = $2;''/eo)
    ||(s/$next_pair_rx/$signature = $2;''/eo));

   $package .= '.' if (length($package) > 0);
   $signature = "($signature)" if (length ($signature) > 0);
   join ('', "{\@link $package$class#$method$signature $class.$method}", $_);
}

sub do_cmd_param {
   local($_) = @_;
   my ($arg, $desc);

   $arg = &missing_braces unless (
    (s/$next_pair_pr_rx/$arg = $2;''/eo)
    ||(s/$next_pair_rx/$arg = $2;''/eo));

   $desc = &missing_braces unless (
    (s/$next_pair_pr_rx/$desc = $2;''/eo)
    ||(s/$next_pair_rx/$desc = $2;''/eo));

   join ('', "\@param $arg $desc\n", $_);
}

sub do_cmd_blocktag {
   local($_) = @_;
   my ($arg, $desc);

   $tagname = &missing_braces unless (
    (s/$next_pair_pr_rx/$arg = $2;''/eo)
    ||(s/$next_pair_rx/$arg = $2;''/eo));

   $desc = &missing_braces unless (
    (s/$next_pair_pr_rx/$desc = $2;''/eo)
    ||(s/$next_pair_rx/$desc = $2;''/eo));

   join ('', "$tagname $desc\n", $_);
}

sub do_cmd_return {
   local($_) = @_;
   my $text;
   $text = &missing_braces unless (
    (s/$next_pair_pr_rx/$text = $2;''/eo)
    ||(s/$next_pair_rx/$text = $2;''/eo));

   join ('', "\@return $text", $_);
}

sub do_cmd_exception {
   local($_) = @_;
   my ($arg, $desc);

   $arg = &missing_braces unless (
    (s/$next_pair_pr_rx/$arg = $2;''/eo)
    ||(s/$next_pair_rx/$arg = $2;''/eo));

   $desc = &missing_braces unless (
    (s/$next_pair_pr_rx/$desc = $2;''/eo)
    ||(s/$next_pair_rx/$desc = $2;''/eo));

   join ('', "\@exception $arg $desc\n", $_);
}

# Some problems arise with math commands
# Some symbols must not be
# displayed as a Unicode char
# because they cause display problems
# with some browsers.
%custom_math_tr = (
   'lfloor', 'floor(',
   'rfloor', ')',
   'lceil', 'ceil(',
   'rceil', ')',
   'approx', '&nbsp;=~&nbsp;',
   'var', 'var',
   'Var', 'Var',
   'le', '&nbsp;&lt;=&nbsp;',
   'leq', '&nbsp;&lt;=&nbsp;',
   'ge', '&nbsp;&gt;=&nbsp;',
   'geq', '&nbsp;&gt;=&nbsp;',
   'to', '&nbsp;-&gt;&nbsp;',
   'htmax', 'max',
   'htmin', 'min',
   'htinf', 'inf',
   'htsup', 'sup',
   'htint', '&int;',
   'htsum', '&sum;',
   'htprod', '&prod;',
   'htlim', 'lim'
);

foreach my $mathcmd (keys %custom_math_tr) {
   eval ("sub do_math_cmd_$mathcmd {\n ('$custom_math_tr{$mathcmd}', \@_);\n}");
}

sub do_math_cmd_bar {
   $arg = &missing_braces unless (
    (s/$next_pair_pr_rx/$arg = $2;''/eo)
    ||(s/$next_pair_rx/$arg = $2;''/eo));

   ("bar($arg)", @_);
}

sub do_math_cmd_overline {
   $arg = &missing_braces unless (
    (s/$next_pair_pr_rx/$arg = $2;''/eo)
    ||(s/$next_pair_rx/$arg = $2;''/eo));

   ("bar($arg)", @_);
}

sub do_math_cmd_hat {
   $arg = &missing_braces unless (
    (s/$next_pair_pr_rx/$arg = $2;''/eo)
    ||(s/$next_pair_rx/$arg = $2;''/eo));

   ("hat($arg)", @_);
}

# Inspired from math.pl
sub do_math_cmd_sqrt {
    local($_) = @_;
    local($n) = &get_next_optional_argument;
    local($surd) = &get_next_token();
    $n ? ("($surd)<SUP>1/$n</SUP>", $_) : ("($surd)<SUP>1/2</SUP>", $_);
}

sub do_env_hide {
   "";
}

sub do_env_detailed {
   "";
}

sub do_env_vcode {
    # Comes from alltt.perl

    local ($_) = @_;
    local($closures,$reopens,$alltt_start,$alltt_end,@open_block_tags);

    if ($HTML_VERSION > 3.0) {
        if ($USING_STYLES) {
            $env_id .= ' CLASS="vcode"' unless ($env_id =~/CLASS=/);
            $env_style{'vcode'} = " " unless ($env_style{'vcode'});
        }
	$alltt_start = "\n<DIV$env_id ALIGN=\"LEFT\">\n";
	$alltt_end = "\n</DIV>\n";
	$env_id = '';
    } else {
	$alltt_start = "<P ALIGN=\"LEFT\">";
	$alltt_end = "</P>";
    }


    # get the tag-strings for all open tags
    local(@keep_open_tags) = @$open_tags_R;
    ($closures,$reopens) = &preserve_open_tags() if (@$open_tags_R);

    # get the tags for text-level tags only
    $open_tags_R = [ @keep_open_tags ];
    local($local_closures, $local_reopens);
    ($local_closures, $local_reopens,@open_block_tags) = &preserve_open_block_tags
	if (@$open_tags_R);

    $open_tags_R = [ @open_block_tags ];

    do {
	local($open_tags_R) = [ @open_block_tags ];
	local(@save_open_tags) = ();

        # preserve the possibility for hiding
        s/\\(begin|end)$O(\d+)${C}hide$O\2$C/<$1_hide>/go;

        # This will prevent braces from being interpreted as
        # arguments, allowing us to display them.
        # This thing comes from latex2html main script, sub
        # revert_to_raw_tex.
        while (s/$O\s*\d+\s*$C/\{/o) { s/$&/\}/;}
        while (s/$O\s*\d+\s*$C/\{/o) { s/$&/\}/;} #repeat this.
        # The same for processed markers ...
        while ( s/$OP\s*\d+\s*$CP/\{/o ) { s/$&/\}/; }
        while ( s/$OP\s*\d+\s*$CP/\{/o ) { s/$&/\}/;} #repeat this.

        # Environments have to be ignored here.
        s/\\(begin|end)//g;
        # Some commands have {} has argument, remove that.
        s/(\\(?:[a-zA-Z]+|[^a-zA-Z])\s*)\{\}/$1/g;

	local($cnt) = ++$global{'max_id'};

        # put back the hide environments.
        s/<(begin|end)_hide>/\\$1$O$cnt${C}hide$O$cnt$C/g;
        $cnt = ++$global{'max_id'};

        # This comes from alltt
	$_ = join('',"$O$cnt$C\\tt$O", ++$global{'max_id'}, $C
		, $_ , $O, $global{'max_id'}, "$C$O$cnt$C");


	$_ = &translate_environments($_);
	$_ = &translate_commands($_) if (/\\/);

	# preserve space-runs, using &nbsp;
	while (s/(\S) ( +)/$1$2;SPMnbsp;/g){};
	s/(<BR>) /$1;SPMnbsp;/g;

#RRM: using <PRE> tags doesn't allow images, etc.
#    $_ = &revert_to_raw_tex($_);
#    &mark_string; # ???
#    s/\\([{}])/$1/g; # ???
#    s/<\/?\w+>//g; # no nested tags allowed
#    join('', $closures,"<PRE$env_id>$_</PRE>", $reopens);
#    s/<P>//g;
#    join('', $closures,"<PRE$env_id>", $_, &balance_tags(), '</PRE>', $reopens);

	$_ = join('', $closures, $alltt_start , $local_reopens
		, $_
		, &balance_tags() #, $local_closures
		, $alltt_end, $reopens);
	undef $open_tags_R; undef @save_open_tags;
    };

    $open_tags_R = [ @keep_open_tags ];
    $_;
}

&ignore_commands ("tab\ntabb\ntabbb\nendtab\nendtabb\nendtabbb\nhrule\n"
           . "cite # [] # {}\n"
           . "eqlabel # {}\n"
           . "ref # {}\n"
           . "defclass # {}\n"
           . "defmodule # {}");

1;
