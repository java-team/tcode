#!/bin/sh

if [ ! $TCODEHOME ]; then
    echo "You must set TCODEHOME before calling this script."
    exit
fi

alias texjava='perl $TCODEHOME/texjava.pl'

if [ $CLASSPATH ]; then
   export CLASSPATH=$TCODEHOME/tcode.jar:$CLASSPATH
else
   export CLASSPATH=$TCODEHOME/tcode.jar
fi

if [ $TEXINPUTS ]; then
   export TEXINPUTS=.:${TCODEHOME}:$TEXINPUTS
else
   export TEXINPUTS=.:${TCODEHOME}:
fi

perl $TCODEHOME/setl2hinit.pl $TCODEHOME
